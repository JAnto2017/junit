package practic_02;

import org.junit.*;

public class SecuenciasTest {
    Secuencias secuencias;

    @BeforeClass
    public static void primero() {
        System.out.println("Se ejecuta al principio de todas las pruebas unitarias, por única vez");
    }

    @AfterClass
    public static void ultimo() {
        System.out.println("Se ejecuta al final de todas las pruebas unitarias, por única vez");
    }
    @BeforeAll
    public void setUp() {
        System.out.println("Se ejecuta al principio de todas las pruebas unitarias");
    }
    @After
    public void tearDown() {
        System.out.println("Se ejecuta al final de todas las pruebas unitarias");
    }
    @Test
    public void test1() {
        System.out.println("Prueba 1");
    }

    
}
