# JUNIT 5 - MOCKITO - SPTRING BOOT

- [JUNIT 5 - MOCKITO - SPTRING BOOT](#junit-5---mockito---sptring-boot)
  - [Introducción](#introducción)
  - [Instalación y configuración en Windows del JDK](#instalación-y-configuración-en-windows-del-jdk)
  - [JUNIT 5](#junit-5)
    - [Inicio Prubas Unitarias con JUnit 5](#inicio-prubas-unitarias-con-junit-5)
      - [JUnit Jupiter](#junit-jupiter)
      - [Anotaciones JUnit Jupiter](#anotaciones-junit-jupiter)
    - [Escribiendo y ejecutandos pruebas unitarias](#escribiendo-y-ejecutandos-pruebas-unitarias)
    - [Test Driven Development TDD con JUnit 5](#test-driven-development-tdd-con-junit-5)
    - [Agregar mensajes en los métodos Assertions](#agregar-mensajes-en-los-métodos-assertions)
    - [Anotaciones DisplayName y Disabled](#anotaciones-displayname-y-disabled)
    - [Ciclo de vida anotaciones](#ciclo-de-vida-anotaciones)
      - [Anotaciones ciclo de vida](#anotaciones-ciclo-de-vida)
    - [Test condicionales](#test-condicionales)
    - [Clases de test anidadas](#clases-de-test-anidadas)
    - [Repetir pruebas](#repetir-pruebas)
    - [Pruebas parametrizadas](#pruebas-parametrizadas)
    - [Test con TAG](#test-con-tag)
    - [Inyección de Dependencias y componentes testinfo](#inyección-de-dependencias-y-componentes-testinfo)
    - [Timeout en JUnit5](#timeout-en-junit5)
    - [Maven surefire plugin](#maven-surefire-plugin)

---

## Introducción

Se integra **JUnit-5** con **Spring Boot** Test y **Mockito**.

## Instalación y configuración en Windows del JDK

Una vez descargada la versión del **JDK**. En **Variables de Entorno** se debe añadir la ruta en variables de usuario o en variables de sistema, en la variable **Path**, incluido hasta la carpeta **\bin**.

Posteriormente se añade la variable de entorno **JAVA_HOME** se escribe en mayúsculas porque es una constante. En esta variable no se incluye la carpeta \bin.

Para probar si está bien configurado, escribir en una consola CMD: **java -version**, y la respuesta debe ser la versión del Java. También podemos comprobar el compilador, escribiendo: **javac --version**.

## JUNIT 5

**JUnit test** es una librería java *framework* para escribir y ejecutar en JVM las pruebas unitarias. En **JUnit 5** (última versión) utiliza programación funcional y lambda, incluyendo varios estilos diferentes de pruebas, configuraciones anotaciones, ciclos de vida, etc.

En **JUnit 5** a diferencia con **JUnit 4**, está en bloques diferenciados:

- JUnit Platform. Librería principal.
- JUnit Jupiter. Sirve para escribir los tests.
- JUnit Vintage. Permite integra la versión 4 con la versión 5.

![Arquitectura JUnit 5](../imagenes/arqjunit5.png)

### Inicio Prubas Unitarias con JUnit 5

1. Diseño.
2. Escribir nuestro código.
3. Probar el código.

Las pruebas unitarias son un proceso de examen para verificar que una pieza de código cumple con ciertas reglas de negocio y afirmar un resultado esperado.

Existen prueba **paramétricas** y pruebas **automatizadas continuas** en el tiempo.

#### JUnit Jupiter

- API para escribir nuestros tests.
- Agrega nuevo modelo y características en JUnit 5.
- Nuevas anotaciones y estilos de testing.
- Permite escribir extensiones.

¡Recuerda! el `org.junit.jupiter:junit-jupiter:5.6.3` incorpora todo, así que en el *Maven* es el que se debe añadir en las dependencias.

#### Anotaciones JUnit Jupiter

- `@Test`.
- `@DisplayName`.
- `@Nested`.
- `@Tag`.
- `@ExtendWith`.
- `@BeforeEach`.
- `@AfterEach`.
- `@BeforeAll`.
- `@AfterAll`.
- `@Disable`.

### Escribiendo y ejecutandos pruebas unitarias

Se crea la clase de pruebas con el mismo nombre que la clase a probar, añadiendo la palabra *Test* en forma *CamelCase*. La clase a probar, tiene un constructor que recibe un String y un dato tipo BigDecimal que se puede enviar con comillas dobles.

```java
import org.junit.jupiter.api.Test;
import java.math.BigDecimal;
import static org.junit.jupiter.api.Assertions.*;
class CuentaTest {
    @Test
    void test() {
        Cuenta cuenta = new Cuenta("Antonio", new BigDecimal("1000.1234"));

        String esperado = "Antonio";
        String actual = cuenta.getPersona();
        Assertions.assertEquals(esperado,actual);
    }
}
```

### Test Driven Development TDD con JUnit 5

Comparar por **referencia**.

```java
@Test
    void testReferenciaCuenta() {
        Cuenta cuenta_actual = new Cuenta("John Doe", new BigDecimal("8900.9997"));
        Cuenta cuenta_esperado = new Cuenta("John Doe", new BigDecimal("8900.9997"));

        assertNotEquals(cuenta_esperado, cuenta_actual);
    }
```

Resultado de la comparación por **referencia** de los objetos, es que cumple.

Si queremos por comparar por **valor** el resultado obtenido es que no cumple por ser distintos. Para ello se utiliza `assertEquals(cuenta_esperado, cuenta_actual);` en lugar del `assserNotEquals`.

![Compara Objetos](../imagenes/resulprueba.png)

### Agregar mensajes en los métodos Assertions

En cada uno de los métodos **Assertions** se pueden añadir mensajes de texto personalizados al final de los atributos de entrada. Por ejemplo, `assertEquals(esperado, actual, "Mensaje personalizado")`.

El *String* del texto, también se puede pasar como una función anónima. Esto mejora en que la instancia del texto, únicamente se crea si es necesario, en caso contrario, siempre se crea la instancia ocupando espacio.

Ejemplo con expresión Lambda: `assertEquals(esperado, actual, () -> "Mensaje personalizado")`.

Si la prueba es correcta, los métodos de la expresión Lambda no se crean ahorrando espacio. Si la prueba es incorrecta si que se crean, generando el texto.

### Anotaciones DisplayName y Disabled

Son anotaciones `@DisplayName` y `@Disable` anotaciones importantes, si queremos dar mayor información a cada método que estamos ejecutando.

```java

    @Test
    @DisplayName("Prueba el nombre de la cuenta corriente")
    void testNombreCuenta() {
        Cuenta cuenta = new Cuenta("Antonio", new BigDecimal("1000.1234"));
        String esperado = "Antonio";
        String actual = cuenta.getPersona();
        assertEquals(esperado,actual, () -> "El nombre de la cuenta no es el esperado");
    }
```

Otra anotación interesante, cuando estamos utilizando el paradigma, **desarrollo impulsado a pruebas** (**TDD**). Podemos usar el método `@Disabled`, para inhabilitar un método de pruebas (no lo ejecuta). Si queremos forzar el error, se utiliza el método `fail()`.

### Ciclo de vida anotaciones

El **ciclo de vida** es un proceso en el cual se crea una instancia. Cuando termina la instancia, al final del proceso, se destruye.

#### Anotaciones ciclo de vida

Orden en la ejecución de las anotaciones:

1. `@BeforeAll`. Se ejecuta una sola vez, al inicio de la prueba.
2. `@AfterAll`. Se ejecuta una sola vez, al final del proceso de pruebas.
3. `@BeforeEach`. Se ejecuta en cada método, antes de que se inicialice.
4. `@AfterEach`. Se ejecuta después de que se haya ejecutado el método, al finalizar.

Ejemplo de uso de las anotaciones:

```java
Cuenta cuenta;

    @BeforeEach
    void initMetodoTest() {
        this.cuenta = new Cuenta("Antonio", new BigDecimal("1000.1234"));
        System.out.println("Iniciando el método de prueba");
    }
    @AfterEach
    void tearDown() {
        System.out.println("Finalizando el método de prueba");
    }
```

Las anotaciones `@AfterAll` y `@BeforeAll` son métodos estáticos, necesitan la instancia. Si queremos que no sean estáticos, que pertenezcan a la clase se debe añadir al inicio antes de declarar la clase, la siguiente línea: `@TestInstance(TestInstance.Lifecycle.PER_CLASS)`.

```java

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CuentaTest {
        
    @BeforeAll
    void beforeAll() {  //NO NECESITAN SER STATIC SI AÑADIMOS @TestInstance

    }

    @AfterAll
    void afterAll() {   //NO NECESITAN SER STATIC SI AÑADIMOS @TestInstance

    }
}
```

Ahora como se maneja una sola instancia para la clase, no se precisa que los métodos sean estáticos.

### Test condicionales

Son pruebas unitarias que se van a ejecutar en ciertos escenarios.

En el ejemplo siguiente, especificamos el OS; así podemos ejecutar ciertas pruebas dependiendo del SO. Otra opción de selección es según la versión del Java JRE v8 (se puede seleccionar otra versión del JRE)

```java

    @Test
    @EnabledOnOs(OS.WINDOWS)
    void testSoloWindows() {
        //código se ejecuta en OS Windows
    }
    @Test
    @EnabledOnOs(OS.LINUX)
    void testSoloLinux() {
        //código se ejecuta en OS Linux
    }
    @Test
    @EnabledOnJre(JRE.JAVA_8)
    void soloJDK8() {
        //si JRE = v8 se ejecuta código
    }
     @Test
    @EnabledIfSystemProperty(named = "java.version", matches = "21.0.1")
    void testJavaVersion() {
        //si JRE = v8 se ejecuta código
    }
    
    @Test
    @EnabledIfSystemProperty(named = "os.arch", matches = ".*64.*")
    void testSolo64bit() {
        //se ejecuta si la arquitectura es de 64 bits
    }
    @Test
    @EnabledIfSystemProperty(named = "os.arch", matches = ".*32.*")
    void testNo64bit() {
        //se ejecuta si la arquitectura no es de 64 bits.
    }
```

Variables del OS. Para listar las varibales definidas en *Variables de entorno*:

```java
 @Test
    void imprimirVariableAmbiente() {
        Map<String, String> getenv = System.getenv();
        getenv.forEach((k,v)-> System.out.println(k + " = " + v));
    }
```

Para ejecutar pruebas unitarias en función del número de procesadores disponibles:

```java
    @Test
    @EnabledIfEnvironmentVariable(named = "NUMBER_OF_PROCESSORS", matches = "12")
    void testProcesadores() {
        //en matches se indican el nº de procesadores
    }
```

Habilitar/deshabilitar si se cumple condición, como un servidor activo o no. Para ello se utiliza lo métodos `import static org.junit.jupiter.api.Assumptions.*;`. **Assumption** es para asumir algún valor.

```java
    @Test
    @DisplayName("prueba con assume en lugar de assert")
    void testUsoAssume() {
        boolean esCierto = true;
        assumeTrue(esCierto);
    }
```

Otra forma de controlar las pruebas (*assert*), es controlando el estado de una variable *booleana*, ejemplo:

```java
@Test
    @DisplayName("Prueba Saldo Cuenta con assuming")
    void testSaldoCuent2a() {
        boolean esCierto = true;
        assumingThat(esCierto, () -> {
            double esperado = 1000.1234;
            double actual = cuenta.getSaldo().doubleValue();
            assertEquals(esperado, actual);
            assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        });
    }
```

### Clases de test anidadas

Usar clases anidadas en la clase de test, permite tener más de una clase en el mismo archivo.

Se tiene que usar la anotación `@Nested` en las clases anidadas.

```java
class principal {
    
    @BeforeAll
    static public void beforeAll() {}

    @AfterAll
    static public void afterAll() {}

    @Nested
    class anidada_uno {
        //pruebas aquí
    }

    @Nested
    class anidada_dos {
        //prueba aqui
    }
}
```

### Repetir pruebas

La anotación `@Test` se reemplaza por `@RepeatedTest(nº de repet.)`.

```java
  @RepeatedTest(value=5, name="Número de repeticiones {currentRepetition} de {totalRepetitions}")
    void pruebaRepetidasTest(RepetitionInfo info) {
        if (info.getCurrentRepetition() == 2){
            //en la repetición dos haces algo
            System.out.println("=====> Estamos repetición: "+info.getCurrentRepetition());
        }
        cuenta.debito(new BigDecimal(100));
        assertNotNull(cuenta.getSaldo());
    }
```

### Pruebas parametrizadas

Sirve para parametrizar un valor a repetir en el método a probar. El valor irá cambiando en cada iteración, facilitando así, comprobar en un solo test, distintos resultados.

Se debe usar la anotación `@ParametrizedTest` en lugar de `@Test`. Además, con la etiqueta `@ValueSource()`para establecer el rango de valores que se quieren utilizar en la prueba.

```java
    @ParameterizedTest
    @ValueSource(strings = {"100","200","300","500","700","1000"})
    @DisplayName("Prueba Parametrizada")
    void testDebitoCuentaParametrizada(String monto) {
        cuenta.debito(new BigDecimal(monto));
        assertNotNull(cuenta.getSaldo());
        assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
    }
```

En la anotación `@ParametrizedTest(name = "El número {index} con valor {0}")`, podemos añadir atributo *name* con el índice y con `{0}` que corresponde con el valor asignado; así informar de cada una de las pruebas.

Alternativa en la anotación `@CsvSource({"1,100", "2,200", "3,300", "4,500"})`, donde tendremos un índice y un valor. Ahora en la función hay que pasar el índice y el valor:

```java
    @ParameterizedTest
    @CsvSource({"1,100","2,200","3,300","4,500","5,700","6,1000"})
    void testDebitoCuentaParametrizada(String index, String monto) {
        System.out.println(index + " -> " + monto);
        cuenta.debito(new BigDecimal(monto));
        assertNotNull(cuenta.getSaldo());
        assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
    }
```

Otro tipo de anotación para pruebas paramétricas, sería con `@CsvFileSource(resources = "/data.csv")`, el archivo con los valores debe estar en la misma carpeta *resources*. Esta prueba no necesita el argumento *index*, solo el *monto*.

Otr alternativa en las prueba paramétrica, es utilizar `@MethodSource("nombre_metod")`, donde se debe especificar el método de entrada.

```java
    @MethodSource("nombre_metod_entrada")
    void testDebitoCuentaParametrizada(String index, String monto) {
        
        assertNotNull();
    }

    private List<String> nombre_metod_entrada() {
        List<String> valores = Arrays.asList("100","200","300");
        return valores;
    }
```

### Test con TAG

Para asignar etiquetas y realizar pruebas selectivas, con determinada etiqueta. Se utiliza la etiqueta `@Tag(String)`.

Con el *string* indicamos qué pruebas quieres realizar. Por ejemplo: `@Tag("param")` especificamos pruebas parametrizadas.

Un método puede tener dos etiquetas, por ejemplo: `@Tag("cuentas")` más `@Tag("banco")`.

Para ejecutar las etiquetas, en *editar pruebas > seleccionar Tags* en lugar de *Class*. Y escribir en el editable, el nombre usado en el *string*. Hacer clic y se ejecutarán únicamente aquellas Tag que coincidan con el nombre.

### Inyección de Dependencias y componentes testinfo

```java
    @Test
    @Tag("etiqueta_uno")
    void usarTestInfoReporter(TestInfo testInfo, TestReporter testReporter) {
        System.out.println("Ejecutando: "+testInfo.getDisplayName());
        System.out.println("Ejecutando: "+testInfo.getTestMethod().orElse(null).getName());
        System.out.println("Etiqueta: "+ testInfo.getTags());
    }
```

El resultado obtenido, es:

```text
    Ejecutando: usarTestInfoReporter(TestInfo 
    TestReporter)Ejecutando: usarTestInfoReporter, Etiqueta: [etiqueta_uno]
    Finalizando el método de prueba.
```

### Timeout en JUnit5

Si un método tarda demasiado tiempo en ejecutarse, salta el error. Es útil para determinar pruebas de rendimiento.

```java
 @Nested
    class tiempoExcedido {
        @Test
        @Timeout(5) //segundos
        void pruebasTimeout() {
            try {
                TimeUnit.SECONDS.sleep(6);  
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        @Test
        void timeoutAssertions() {
            assertTimeout(Duration.ofSeconds(5), ()->{
                TimeUnit.SECONDS.sleep(4);
            });
        }
    }//class tiempoExcedido
```

### Maven surefire plugin

Para ejecutar las pruebas mediante una plataforma de consola, sin necesidad de un IDE.

En el **pom.xml** se debe agregar en la raíz el *plugin template > mave-surefire-plugin*.

```xml
<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>3.1.2</version>
    </plugin>
</plugins>
```

Para limitar las pruebas unitarias a lo especificado en las estiquetas, se debe añadir al fichero *pom.xml* el siguiente código:

```xml
<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>3.1.2</version>
        <configuration>
            <groups>timeout</groups>
        </configuration>
    </plugin>
</plugins>
```

Donde se especifica en *groups* la etiqueta que queremos probar en las pruebas, ignorando el resto.

Desde la página web de **[Maven](https://maven.apache.org/)** descargar el binario para el OS y descomprimir.

Copiar la ruta *maven/bin* y añadirla al PATH a las variables del sistema. Se debe crear MAVEN_HOME también.

Para realizar las pruebas unitarias con **Maven**, ejecutar `mvn test` dentro de la carpeta donde está en fichero *pom.xml*.

[README](../README.md)
