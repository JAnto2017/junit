package org.janto.appmockito.ejemplos.repositories;

import org.janto.appmockito.ejemplos.models.Examen;

import java.util.List;

public interface ExamenRepository {
    List<Examen> findAll();
}
