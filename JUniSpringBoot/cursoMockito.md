# MOCKITO

- [MOCKITO](#mockito)
  - [Introducción a Mockito](#introducción-a-mockito)
  - [Configurar JUnit-5 y Mockito](#configurar-junit-5-y-mockito)

---

## Introducción a Mockito

**Mockito** es un **framework** de pruebas que nos permite crear objetos simulados (**mock**) en un entorno controlado y determinado. A estos **mock** se le pueden dar un comportamiento desseado.

**BDD** (Behavior Driven Development) o desarrollo impulsado por el comportamiento. Existe el desarrollo orientado a las pruebas unitarias **TDD**.

## Configurar JUnit-5 y Mockito

[README](../README.md)