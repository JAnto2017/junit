package org.example.example;

import org.example.exceptions.DineroInsuficienteException;
import java.math.BigDecimal;

public class Cuenta {
    public Cuenta(String persona, BigDecimal saldo) {
        this.persona = persona;
        this.saldo = saldo;
    }
    //-----------------------------------------------------------------------
    //Atributos
    private Banco banco;
    private String persona;
    private BigDecimal saldo;
    //------------------------------------------------------------------------
    //Setters & Getters
    public String getPersona() {return persona;}
    public void setPersona(String persona) {this.persona = persona;}
    public BigDecimal getSaldo() {return saldo;}
    public void setSaldo(BigDecimal saldo) {this.saldo = saldo;}
    public Banco getBanco() {
        return banco;
    }
    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    //------------------------------------------------------------------------
    //Métodos
    public void debito(BigDecimal monto){
        BigDecimal nuevoSaldo = this.saldo.subtract(monto);
        if (nuevoSaldo.compareTo(BigDecimal.ZERO) < 0){
            throw new DineroInsuficienteException("Dinero Insuficiente");
        }
        this.saldo = nuevoSaldo;
    }
    public void credito(BigDecimal monto){this.saldo = this.saldo.add(monto);}

    /**
     * Sirve para realizar la comparación de contenido del objeto
     * y si es igual pasa la prueba y si es diferente no.
     * Usando assertEquals()
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Cuenta)){
            return false;
        }
        Cuenta c = (Cuenta) obj;
        if (this.persona == null || this.saldo == null){
            return false;
        }
        return this.persona.equals(c.getPersona()) && this.saldo.equals(c.getSaldo());
    }
}