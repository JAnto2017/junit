package org.example.example;

import org.example.exceptions.DineroInsuficienteException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CuentaTest {
    Cuenta cuenta;

    @BeforeEach
    void initMetodoTest() {
        this.cuenta = new Cuenta("Antonio", new BigDecimal("1000.1234"));
    }

    @AfterEach
    void tearDown() {
        System.out.println("Finalizando el método de prueba");
    }

    @BeforeAll
    void beforeAll() {  //NO NECESITAN SER STATIC SI AÑADIMOS @TestInstance
    }

    @AfterAll
    void afterAll() {   //NO NECESITAN SER STATIC SI AÑADIMOS @TestInstance
    }

    @Nested
    class anidada_uno {
        @Test
        @DisplayName("Prueba el nombre de la cuenta corriente")
        void testNombreCuenta() {

            String esperado = "Antonio";
            String actual = cuenta.getPersona();
            assertEquals(esperado,actual, () -> "El nombre de la cuenta no es el esperado");
        }
        @Test
        @DisplayName("Prueba Saldo Cuenta")
        void testSaldoCuenta() {
            double esperado = 1000.1234;
            double actual = cuenta.getSaldo().doubleValue();
            assertEquals(esperado, actual);
            assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }
    }

    @Nested
    class anidada_dos {
        @Test
        @DisplayName("Prueba Referencias de Cuenta")
        void testReferenciaCuenta() {
            Cuenta cuenta_actual = new Cuenta("John Doe", new BigDecimal("8900.9997"));
            Cuenta cuenta_esperado = new Cuenta("John Doe", new BigDecimal("8900.9997"));

            assertEquals(cuenta_esperado, cuenta_actual);
        }
        @Test
        @DisplayName("Pruebas Debito de Cuenta")
        void testDebitoCuenta() {
            cuenta.debito(new BigDecimal(100));
            int esperado = 900;
            int actual = cuenta.getSaldo().intValue();
            assertNotNull(cuenta.getSaldo());
            assertEquals(esperado,actual);
            assertEquals("900.1234", cuenta.getSaldo().toPlainString());
        }
    }

    @Nested
    class anidada_tres {
        @Test
        @DisplayName("Prueba Crédito de Cuenta")
        void testCreditoCuenta() {
            cuenta.credito(new BigDecimal(100));
            int esperado = 1100;
            int actual = cuenta.getSaldo().intValue();
            assertNotNull(cuenta.getSaldo());
            assertEquals(esperado,actual);
            assertEquals("1100.1234", cuenta.getSaldo().toPlainString());
        }

        @Test
        @DisplayName("Prueba Excepción Dinero Insuficiente en Cuenta ")
        void testDineroInsuficienteExceptionCuenta() {
            Exception exception = assertThrows(DineroInsuficienteException.class, ()->{
                cuenta.debito(new BigDecimal(1500));
            });
            String actual = exception.getMessage();
            String esperado = "Dinero Insuficiente";
            assertEquals(esperado,actual);
        }
    }

    @Nested
    class anidada_cuatro {
        @Test
        @DisplayName("Prueba Transferir Dinero a Cuenta")
        void testTransferirDineroCuentas() {
            Cuenta cuenta1 = new Cuenta("Jhon Doe", new BigDecimal("2500"));
            Cuenta cuenta2 = new Cuenta("Andres", new BigDecimal("1500.8989"));

            Banco banco = new Banco();
            banco.setNombre("Banco del Estado");
            banco.transferir(cuenta2, cuenta1, new BigDecimal(500));

            assertEquals("1000.8989", cuenta2.getSaldo().toPlainString());
            assertEquals("3000",cuenta1.getSaldo().toPlainString());
        }

        @Test
        @DisplayName("Prueba Relación Banco con Cuenta")
        void testRelacionBancoCuentas() {
            Cuenta cuenta1 = new Cuenta("Jhon Doe", new BigDecimal("2500"));
            Cuenta cuenta2 = new Cuenta("Andres", new BigDecimal("1500.8989"));

            Banco banco = new Banco();
            banco.addCuenta(cuenta1);
            banco.addCuenta(cuenta2);

            banco.setNombre("Banco del Estado");
            banco.transferir(cuenta2, cuenta1, new BigDecimal(500));

            assertAll(()->{
                assertEquals("1000.8989", cuenta2.getSaldo().toPlainString(),
                        ()->"El valor de CUENTA-2 no es el esperado");
            },()->{
                assertEquals("3000",cuenta1.getSaldo().toPlainString(),
                        ()->"El valor de CUENTA-2 no es el esperado");
            },()->{
                assertEquals(2, banco.getCuentas().size());
            },()->{
                assertEquals("Banco del Estado", cuenta1.getBanco().getNombre());
            },()->{
                assertEquals("Andres", banco.getCuentas().stream()
                        .filter(c -> c.getPersona().equals("Andres"))
                        .findFirst()
                        .get().getPersona());
            },()->{
                assertTrue(banco.getCuentas().stream()
                        .anyMatch(c -> c.getPersona().equals("Jhon Doe")));
            });
        }
    }


    @Nested
    class SistemaOperativoTest {
        @Test
        @EnabledOnOs(OS.WINDOWS)
        void testSoloWindows() {
        }
        @Test
        @EnabledOnOs(OS.LINUX)
        void testSoloLinux() {
        }
    }

    @Nested
    class propiedadesDeSistemaTest {
        @Test
        void imprimirSystemProperties() {
            Properties properties = System.getProperties();
            properties.forEach((k,v)-> System.out.println(k + ":" + v));
        }
        @Test
        @EnabledIfSystemProperty(named = "java.version", matches = "21.0.1")
        void testJavaVersion() {
        }
        @Test
        @EnabledIfSystemProperty(named = "os.arch", matches = ".*64.*")
        void testSolo64bit() {
            //se ejecuta si la arquitectura es de 64 bits
        }
        @Test
        @EnabledIfSystemProperty(named = "os.arch", matches = ".*32.*")
        void testNo64bit() {
            //se ejecuta si la arquitectura no es de 64 bits.
        }
    }

    @Nested
    class javaVersionTest {
        @Test
        @EnabledIfEnvironmentVariable(named = "JAVA_HOME", matches = "jdk-21" )
        void testJavaHome() {
        }
        @Test
        @EnabledOnJre(JRE.JAVA_8)
        void soloJDK8() {
        }
    }

    @Nested
    class variablesEntornoTest {
        @Test
        void imprimirVariableAmbiente() {
            Map<String, String> getenv = System.getenv();
            getenv.forEach((k,v)-> System.out.println(k + " = " + v));
        }
        @Test
        @EnabledIfEnvironmentVariable(named = "NUMBER_OF_PROCESSORS", matches = "12")
        void testProcesadores() {
        }
        @Test
        @DisplayName("prueba con assume en lugar de assert")
        void testUsoAssume() {
            boolean esCierto = true;
            assumeTrue(esCierto);
        }
        @Test
        @DisplayName("Prueba Saldo Cuenta con assuming")
        void testSaldoCuent2a() {
            boolean esCierto = true;
            assumingThat(esCierto, () -> {
                double esperado = 1000.1234;
                double actual = cuenta.getSaldo().doubleValue();
                assertEquals(esperado, actual);
                assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
                assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
            });
        }
    }

    @Nested
    class PruebasParametrizadasTest {
        @RepeatedTest(value=5, name="Repetición {currentRepetition} de {totalRepetitions}")
        void pruebaRepetidasTest(RepetitionInfo info) {
            if (info.getCurrentRepetition() == 2){
                //en la repetición dos haces algo
                System.out.println("=====> Estamos repetición: "+info.getCurrentRepetition());
            }
            cuenta.debito(new BigDecimal(100));
            assertNotNull(cuenta.getSaldo());
        }

        @ParameterizedTest(name="número {index} ejecutando {0} valor con {argumentsWithNames}")
        @ValueSource(strings = {"100","200","300","500","700","1000"})
        @DisplayName("Prueba Parametrizada")
        void testDebitoCuentaParametrizada(String monto) {
            cuenta.debito(new BigDecimal(monto));
            assertNotNull(cuenta.getSaldo());
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }
        @Test
        @Tag("etiqueta_uno")
        void usarTestInfoReporter(TestInfo testInfo, TestReporter testReporter) {
            System.out.println("Ejecutando: "+testInfo.getDisplayName());
            System.out.println("Ejecutando: "+testInfo.getTestMethod().orElse(null).getName());
            System.out.println("Etiqueta: "+ testInfo.getTags());
        }
    }//class PruebasParametrizadasTest
    @Nested
    class tiempoExcedido {
        @Test
        @Timeout(3) //segundos
        void pruebasTimeoutseg() {
            try {
                TimeUnit.SECONDS.sleep(3);  //segundos
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        @Test
        @Timeout(value=2, unit=TimeUnit.MILLISECONDS)
        void pruebasTimeoutms() {
            try {
                TimeUnit.MILLISECONDS.sleep(2);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        @Test
        void timeoutAssertions() {
            assertTimeout(Duration.ofSeconds(2), ()->{
                TimeUnit.SECONDS.sleep(1);
            });
        }
    }//class tiempoExcedido
}//class CuentaTest
