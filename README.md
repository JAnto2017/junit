# JUnit 5

- [JUnit 5](#junit-5)
  - [Antecedentes](#antecedentes)
  - [Introducción a JUnit 5](#introducción-a-junit-5)
  - [Proyecto base](#proyecto-base)
  - [Ciclo de vida de una prueba unitaria](#ciclo-de-vida-de-una-prueba-unitaria)
    - [JUNIT 4](#junit-4)

---

## Antecedentes

- JUnit como *framework* para hacer pruebas unitarias.
- Anotación `@Test` para ejecutar una prueba unitaria.
- Métodos de aserciones cómo: *assertEquals(), assertNotEquals(), assertTrue(), assertFalse()*.
- Etiquetas `@Disabled` y `@DisplayName`.
- Ciclo de vida de una prueba unitaria.
- Probar excepciones con *assertThrows* y *assertNotThrows()*.

---

## Introducción a JUnit 5

Si tenemos una clase *Calculadora* como la siguiente, en la que hay dos métodos a probar:

    public class Calculadora {
        float sumar(float x, float y) {
            return x + y;
        }
        int dividir(int x, int y){
            return x/y;
        }
    }

La prueba unitaria con **JUnit 5** se realiza sobre la clase *Calculadora* de la siguiente forma:

    public class CalculadoraTest {
        @Test
        void additionTest() {
            Calculadora calculadora = new Calculadora();
            float valorEsperado = 2;
            assertEquals(valorEsperado, calculadora.sumar(1,1));
        }
    }

Principales anotaciones: `@Test`, `@BeforeEach`, `@Disabled("este test no se ejecuta)`, `AfterEach`.

## Proyecto base

Para comentar las prubas unitarias, tenemos: `@DisplayName("test para el método dividir")`. Se coloca antes del método de pruebas.

    @Test
    @DisplayedName("test para método dividir")
    public void dividirTest() {
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.dividir(2, 2);
        int valorEsperado = 0;

        assertFalse(resultado == 0);
        //assertTrue(resultado != 0);
        assertNotEquals(valorEsperado, resultado);
    }

Otra etiqueta para deshabilitar una prueba unitaria, es: `@Disabled("este test no se realiza por el momento")`. Se coloca antes del método de pruebas.

## Ciclo de vida de una prueba unitaria

Necesidad de crear, inicializar y limpiar objetos o variables dentro de cada prueba unitaria o en general para todas las pruebas unitarias.

Las anotaciones en ciclo de vida de una prueba unitaria:

- 1ª en ejecutarse es `@BeforeAll`. Se jecuta al inicio de todas las prueba unitarias y sólo una vez.
- 2ª en ejecutarse es `@BeforeEach`. Se ejecuta al inicio de cada prueba unitaria.
- 3ª en ejecutarse es `@Test`.
- 4º en ejecutarse es `@AfterEach`. Se ejecuta al final de cada prueba unitaria.
- 5ª en ejecutarse es `@AfterAll`. se ejecuta al final de todas las pruebas unitarias y sólo una vez.

### JUNIT 4

| Anotación | Descripción |
| :---: | :---: |
| `@Test` | Método de test. |
| `@Before` | Se ejecuta antes de cada método de test. |
| `@After` | Se ejecuta después de cada método de test. |
| `@BeforeClass` | Se ejecuta solo una vez, antes de la ejecución de todos los tests. |
| `@AfterClass` |   Se ejecuta solo una vez, después de la ejecución de todos los tests. |
| `@Ignore` | Indica a JUnit que el método de test está deshabilitado. |
| `@Test (expected = Exception.class)` | Fails if the method does not throw the named exception. |
| `@Test (timeout=100)` | Fails if the method takes longer than 100 milliseconds |

[curso JUnit 5](/JUniSpringBoot/cursojunit5.md)<br>
[Curso Mockito](/JUniSpringBoot/cursoMockito.md)<br>