package practic_01;

public @interface Disabled {

    String value();

}
