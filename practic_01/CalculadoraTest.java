package practic_01;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
//import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;

import org.junit.Test;


public class CalculadoraTest {
    @Test
    public void sumarTest() {
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.sumar(2, 2);
        int valorEsperado = 4;
        assertEquals(valorEsperado, resultado);
        assertNotEquals(3, resultado);
    }

    @Test
    public void restarTest() {
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.restar(2, 2);
        int valorEsperado = 0;

        assertEquals(valorEsperado, resultado);
    }

    @Test
    public void multiplicarTest() {
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.multiplicar(2, 2);
        int valorEsperado = 4;

        assertEquals(valorEsperado, resultado);
    }

    @Test
    public void dividirTest() throws Exception {
        Calculadora calculadora = new Calculadora();
        int resultado = calculadora.dividir(2, 2);
        int valorEsperado = 0;

        //assertFalse(resultado == 0);
        //assertTrue(resultado != 0);
        assertNotEquals(valorEsperado, resultado);
    }

    /**
     * Comparando Arrays.
     */
    @Test
    public void arregloTest() {
        //Calculadora calculadora = new Calculadora();
        String [] array1 = {"a","b"};
        String [] array2 = {"c","b"};
        assertArrayEquals(array1, array2);
    }

    /**
     * Comparando Objetos.
     */
    @Test
    public void sameAndNotSameTest() {
        String str1 = "mi cadena";
        String str2 = "mi cadena";

        assertSame(str1, str2);
        assertNotSame(str1, str2);
    }

    /**
     * Comprueba Excepciones.
     */
    @Test
    public void dividirExceptionTest() {
        Calculadora calculadora = new Calculadora();
        
        assertThrows(Exception.class, () -> {calculadora.dividir(10, 0); });
    }
}
