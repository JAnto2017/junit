package practic_01;


public class Calculadora {
    int sumar(int x, int y) {
        return x + y;
    }
    int restar(int x, int y) {
        return x - y;
    }
    int multiplicar(int x, int y) {
        return x * y;
    }
    int dividir(int x, int y) throws Exception {
        if (y==0) {
            throw new Exception("Division by zero");
        } 
        return x/y;
    }
}